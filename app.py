try:
    import argparse
    from sys import exit
    import progressbar
    from pyfiglet import Figlet
    from os import system, getuid
    from time import sleep
    from colorama import Fore, Back, Style, init
except ImportError:
    print("Modüller dahil edilirken bir hata oluştu...")


# sudo kontrolü
if getuid() != 0:
    print("Lütfen uygulamayı Super User olarak kullanın.")
    sleep(1.5)
    system("clear")
    exit()

# progress bar
def myProgressBar():
    for i in progressbar.progressbar(range(100)):
        sleep(0.02)


# app's banner
def banner():
    system("clear")
    customBanner = Figlet(font="graffiti")
    print(customBanner.renderText("Formatter"))


def formatBlack():
    myProgressBar()
    banner()
    folder = input("Dosyanın dizinini girin(örn: /home/user/Documents/main.py): ")
    if folder == "":
        print("Değer boş olamaz.")
        formatBlack()
    elif folder != "":
        system("black " + folder)


def formatAutopep8():
    myProgressBar()
    banner()
    folder = input("Dosyanın dizinini girin(örn: /home/user/Documents/main.py): ")
    if folder == "":
        print("Değer boş olamaz")
        formatAutopep8()
    elif folder != "":
        system("autopep8 --in-place " + folder)


def formatYapf():
    myProgressBar()
    banner()
    folder = input("Dosyanın dizinini girin(örn: /home/user/Documents/main.py): ")
    if folder == "":
        print("Değer boş olamaz.")
        formatYapf()
    elif folder != "":
        system("yapf --style='{based_on_style: pep8, indent_width: 2}' -i " + folder)


def formatIsort():
    myProgressBar()
    banner()
    folder = input("Dosyanın dizinini girin(örn: /home/user/Documents/main.py): ")

    if folder == "":
        print("Değer boş olamaz.")
        formatIsort()
    elif folder == "q":
        main()
    elif folder != "":
        system("isort " + folder)


def main():
    while True:
        myProgressBar()
        banner()
        print(
            """
        \n
        Uygulamaya Hoşgeldiniz :) Lütfen aşağıdan bir 'formatter' seçin.
        \n
        1)Black
        2)Autopep8
        3)Yapf
        4)Isort
        """
        )
        select = input("Seçiminizi girin >> ")

        if select == "":
            print("Değer boş olamaz.")
            main()
        elif select == "1":
            formatBlack()
        elif select == "2":
            formatAutopep8()
        elif select == "3":
            formatYapf()
        elif select == "4":
            formatIsort()


main()
