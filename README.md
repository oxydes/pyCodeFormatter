# Python Code Formatter

Uygulama en çok kullanılan 4 adet python kod formatlayıcısını kullanıyor. Kod formatlayıcıyı seçmek kullanıcının elinde, istediğini seçebilir.

# Kullanılan Formatterlar

1- Black(https://pypi.org/project/black/)

2- Yapf(https://pypi.org/project/yapf/)

3- Autopep8(https://pypi.org/project/autopep8/)

4- isort(https://pypi.org/project/isort/)

____________________________________________________________________________________

Uygulamayı root olarak çalıştırmak istemiyorsanız(neden yazdığımı sorarsanız ben de bilmiyorum);


    if getuid() != 0:
        print("Lütfen uygulamayı Super User olarak kullanın.")
        sleep(1.5)
        system("clear")
        exit()
    

Kod parçacığını silin.
____________________________________________________________________________________

Ya da 

    fakeroot python3 app.py

______________________________________________________________________________________



